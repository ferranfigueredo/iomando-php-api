<?php

namespace Ferranfg\Iomando;

use Illuminate\Support\ServiceProvider;

class IomandoServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('iomando', function($app)
        {
            return new Iomando(env('IOMANDO_SSID'));
        });
    }

}