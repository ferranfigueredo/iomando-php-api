<?php

namespace Ferranfg\Iomando;

class Parking
{
    protected $api;

    public function __construct(Iomando $api)
    {
        $this->api = $api;
    }

    public function listAll($hash)
    {
        return $this->api->request('parking_renting.listAll', [
            'hash' => (string) $hash
        ]);
    }

    public function open($hash, $sms_code, $renting, $door)
    {
        return $this->api->request('parking.open', [
            'hash'     => (string) $hash,
            'sms_code' => (string) $sms_code,
            'renting'  => (string) $renting,
            'door'     => (string) $door
        ]);
    }

}