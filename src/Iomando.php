<?php

namespace Ferranfg\Iomando;

use Exception;
use GuzzleHttp\Client;

class Iomando
{
    const API_URL = 'http://backend.iomando.com/wsapi3.php/method/';

    private $ssid;

    protected $client;

    protected $services;

    public function __construct($ssid)
    {
        $this->ssid   = $ssid;
        $this->client = new Client;
    }

    public function request($uri, array $params = [])
    {
        try
        {
            $params  = array_merge(['ssid' => $this->ssid], $params);
            $request = $this->client->request('POST', self::API_URL . $uri, [
                'form_params' => [
                    'request' => json_encode($params)
                ]
            ]);
            $response = json_decode($request->getBody(), true);

            if (is_array($response) and array_key_exists('error', $response))
            {
                throw new Exception($response['error']['message']);
            }

            return $response;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function user()
    {
        return $this->getService('user');
    }

    public function parking()
    {
        return $this->getService('parking');
    }

    private function getService($name)
    {
        if (isset($this->services[$name])) return $this->services[$name];

        $serviceName = __NAMESPACE__ . '\\' . ucfirst($name);

        $this->services[$name] = new $serviceName($this);

        return $this->services[$name];
    }

    public function __get($name)
    {
        if (in_array($name, ['user', 'parking'], true))
        {
            return $this->getService($name);
        }

        $trace = debug_backtrace();

        $error = 'Undefined property via __get(): %s in %s on line %u';

        trigger_error(sprintf($error, $name, $trace[0]['file'], $trace[0]['line']), E_USER_NOTICE);

        return;
    }

}