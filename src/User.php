<?php

namespace Ferranfg\Iomando;

class User
{
    protected $api;

    public function __construct(Iomando $api)
    {
        $this->api = $api;
    }

    public function register($prefix, $phone)
    {
        return $this->api->request('user.register', [
            'prefix' => (string) $prefix,
            'phone'  => (string) $phone,
            'imei'   => ''
        ]);
    }

    public function login($prefix, $phone, $sms_code)
    {
        return $this->api->request('user.login', [
            'prefix'   => (string) $prefix,
            'phone'    => (string) $phone,
            'imei'     => '',
            'sms_code' => (string) $sms_code
        ]);
    }

    public function set($id, $hash, $sms_code, $email, $open_code)
    {
        return $this->api->request('user.set', [
            'id'        => (string) $id,
            'hash'      => (string) $hash,
            'sms_code'  => (string) $sms_code,
            'email'     => (string) $email,
            'open_code' => (string) $open_code
        ]);
    }

    public function info($id, $hash, $sms_code)
    {
        return $this->api->request('user.info', [
            'id'       => (string) $id,
            'hash'     => (string) $hash,
            'sms_code' => (string) $sms_code,
        ]);
    }

    public function logout($id, $hash, $sms_code)
    {
        return $this->api->request('user.logout', [
            'id'       => (string) $id,
            'hash'     => (string) $hash,
            'sms_code' => (string) $sms_code
        ]);
    }

    public function delete($prefix, $phone, $sms_code)
    {
        return $this->api->request('user.delete', [
            'prefix'   => (string) $prefix,
            'phone'    => (string) $phone,
            'sms_code' => (string) $sms_code,
            'hash'     => '',
        ]);
    }

    public function book($userId, $parking_id, $group_id)
    {
        return $this->api->request('user.book', [
            'user'           => $userId,
            'space'          => $parking_id,
            'group'          => $group_id,
            'date_in_day'    => '01',
            'date_in_month'  => '01',
            'date_in_year'   => '2016',
            'date_out_day'   => '31',
            'date_out_month' => '12',
            'date_out_year'  => '2026',
            'time_in_hour'   => '00',
            'time_in_min'    => '00',
            'time_out_hour'  => '23',
            'time_out_min'   => '59'
        ]);
    }

}