<?php

namespace Ferranfg\Iomando;

use Illuminate\Support\Facades\Facade;

class IomandoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'iomando';
    }
}
